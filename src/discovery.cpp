/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <unistd.h>
#include <signal.h>
#include <stdio.h>

#include "client_disc_manager.h"
#include "softbus_common.h"
#include "softbus_interface.h"

static const char *g_pkgName = "Softbus_Kits";

static bool found = false;

static void DeviceFound(const DeviceInfo *device)
{
    printf("Found a device:\n");
    printf("Device id: %s \n", device->devId);
    printf("Account hash code: %s \n", device->hwAccountHash);
    printf("device type: %d \n", device->devType);
    for (int i=0; i<device->addrNum; i++)
        printf("addr: %s:%d \n", device->addr[i].addr, device->addr[i].port);
    for (int i=0; i<device->capabilityBitmapNum; i++)
        printf("capabilityBitmap: %x \n", device->capabilityBitmap[i]);
    printf("custData: %s \n", device->custData);
    printf("\n");

    found = true;
}

static void DiscoverFailed(int subscribeId, DiscoveryFailReason failReason)
{
    printf("Discovery failed\n");
}

static void DiscoverySuccess(int subscribeId)
{
    printf("Discovery success\n");
}

static IDiscoveryCallback g_subscribeCb = {
    .OnDeviceFound = DeviceFound,
    .OnDiscoverFailed = DiscoverFailed,
    .OnDiscoverySuccess = DiscoverySuccess
};

static SubscribeInfo g_sInfo = {
    .subscribeId = 1,
    .mode = DISCOVER_MODE_ACTIVE,
    .medium = COAP,
    .freq = MID,
    .isSameAccount = true,
    .isWakeRemote = false,
    .capability = "dvKit",
    .capabilityData = (unsigned char *)"capdata1",
    .dataLen = sizeof("capdata1"),
};

volatile sig_atomic_t stop;
void inthand(int signum) {
    stop = 1;
}

int main(int argc, char **argv)
{
    int ret;

    ret = StartDiscovery(g_pkgName, &g_sInfo, &g_subscribeCb);
    if (ret != 0) {
        printf("Start discovery failed: ret = %d \n", ret);
    }

    signal(SIGINT, inthand);
    while(!found && !stop) {
        printf("Looking for devices...\n");
        sleep(1);
    }

    StopDiscovery(g_pkgName, 1);
    if (ret != 0) {
        printf("Stop discovery failed: ret = %d \n", ret);
    }

    return 0;
}