/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <unistd.h>
#include <signal.h>
#include <stdio.h>

#include "client_disc_manager.h"

static const char *g_pkgName = "Softbus_Kits";

static void PublishSuccess(int publishId)
{
    printf("Publish success\n");
}

static void PublishFail(int publishId, PublishFailReason reason)
{
    printf("Publish failed\n");
}

static IPublishCallback g_publishCb = {
    .OnPublishSuccess = PublishSuccess,
    .OnPublishFail = PublishFail
};

static PublishInfo g_pInfo = {
    .publishId = 1,
    .mode = DISCOVER_MODE_PASSIVE,
    .medium = COAP,
    .freq = MID,
    .capability = "dvKit",
    .capabilityData = (unsigned char *)"capdata1",
    .dataLen = sizeof("capdata1")
};

volatile sig_atomic_t stop;
void inthand(int signum) {
    stop = 1;
}

int main(int argc, char **argv)
{
    int ret;

    ret = PublishService(g_pkgName, &g_pInfo, &g_publishCb);
    if (ret != 0) {
        printf("Publish service failed: ret = %d", ret);
    }

    signal(SIGINT, inthand);
    while(!stop) {
        sleep(1);
    }

    ret = UnPublishService(g_pkgName, 1);
    if (ret != 0) {
        printf("UnPublish service failed: ret = %d", ret);
    }

    return 0;
}