# dsoftbus-example
DSoftBus is the communication layer for the distributed capabilities at the base of OpenHarmony and HarmonyOS and provides APIs for device discovery, connection, and data transmission. More information can be found [here](https://gitee.com/openharmony/communication_dsoftbus).

## Device discovery
For the distributed capabilities and cooperation between devices DSoftBus provides a discovery mechanism based on CoAP. The CoAP protocol supports lightweight and reliable transmission which makes it suitable for resource-constrained IoT devices.

The assumption of this discovery mechanism is that the devices are in the same local area network and can receive each other's messages.
The process is the following:

- The discovering end device that wishes to discover the devices that are present in the network, sends broadcasts using the CoAP protocol;
- After the receiving end receives the broadcast, it sends a CoAP  unicast message to the discovering end; 
- The discovering end device receives the reply unicast message and updates the discovered devices information.

## System Abilities
DSoftBus is implemented as a System Ability (SA) in OpenHarmony using the [system ability framework](https://gitee.com/openharmony/distributedschedule_safwk). A System Ability is a process that provides specific capabilities and that you can communicate via [inter-process communication (IPC)](https://gitcode.net/openharmony/communication_ipc). System Abilities are registered with the system ability manager (SAMgr), which manages the SAs and provides APIs for the client. 

Note: OpenHarmony uses Android Binder IPC to implement cross-process communication, therefore it has to be enabled in the kernel configs:
```
CONFIG_ANDROID=y
CONFIG_ANDROID_BINDER_IPC=y
CONFIG_ANDROID_BINDER_DEVICES=binder,hwbinder,vndbinder
```

## Setup and build
To setup the build environment, you first have to clone bitbake,
openembedded-core and extra layers needed. A repo manifest is included in
meta-openharmony that allows to do this with two commands, which must be run
in a newly created directory where you want the build environment to be
placed:
```console
$ mkdir ohoe && cd ohoe
$ repo init -u git@git.ostc-eu.org:francesco.pham/meta-openharmony.git -b dsoftbus
$ repo sync --no-clone-bundle
```
initialize the build environment
```console
$ TEMPLATECONF=../meta-openharmony/conf source oe-core/oe-init-build-env
```

Build an image with the `yocto-openharmony` package:
```console
$ DISTRO=yocto-openharmony-v3 MACHINE=qemuarm bitbake yocto-openharmony-image
```
run the qemu image:
```console
$ DISTRO=yocto-openharmony-v3 MACHINE=qemuarm runqemu yocto-openharmony-image
```

## Run softbus server SA
In order to run DSoftBus the `samgr` process needs to be running as well as the foundation system abilities. To run system abilities, OpenHarmony provides a `sa_main` binary which takes as an argument the path of the system ability profile which is an .xml file describing the system ability and its dependencies. The system abilities profiles original directory was `/system/profile` but we moved the content of this directory into `/usr/share`. 

The commands to start softbus server  in the background look as follows:
```console
$ samgr &
$ sa_main /usr/share/foundation.xml &
$ sa_main /usr/share/softbus_server.xml &
```

## Run 
When softbus server is correctly running in the system you can run this example. Execute `discovery` in one device and `publish_service` in another device, if the discovery is successful it will print out the discovered device and its capabilities.

The following are some packets captured by tcpdump, the device with ip `192.168.7.12` is the discovering device and the device with ip `192.168.7.11` is the device that is publishing the service:
```
DISCOVERY:
15:30:23.466449 IP 192.168.7.12.5684 > 192.168.7.255.5684: UDP, length 304
E..L..@.@............4.4.8..P..-=.192.168.7.255..device_discover.{"deviceId":"{\"UDID\":\"ABCDEF00ABCDEF00ABCDEF00ABCDEF00ABCDEF00ABCDEF00ABCDEF00ABCDEF00\"}","devicename":"UNKNOWN","type":0,"hicomversion":"hm.1.0.0","mode":1,"deviceHash":"","serviceData":"","wlanIp":"192.168.7.12","coapUri":"coap://192.168.7.12/device_discover"}.

DISCOVERY REPLY:
15:37:22.159580 IP 192.168.7.11.5684 > 192.168.7.12.5684: UDP, length 285
E..9d,@.@.F .........4.4.%..@.".<192.168.7.12..device_discover.{"deviceId":"{\"UDID\":\"ABCDEF00ABCDEF00ABCDEF00ABCDEF00ABCDEF00ABCDEF00ABCDEF00ABCDEF00\"}","devicename":"UNKNOWN","type":0,"hicomversion":"hm.1.0.0","mode":1,"deviceHash":"","serviceData":"port:0,","wlanIp":"192.168.7.11","capabilityBitmap":[32]}.
15:37:22.167802 IP 192.168.7.12.5684 > 192.168.7.11.5684: UDP, length 4
```

## Notes
The system must have network connection with an IP assigned. The network interface names are hardcoded and need to be replaced with the correct names in the following files: `foundation/communication/dsoftbus/components/nstackx/nstackx_ctrl/core/nstackx_device.c` and `foundation/communication/dsoftbus/core/bus_center/lnn/net_builder/ip/ip_hook.c`