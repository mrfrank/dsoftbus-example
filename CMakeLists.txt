cmake_minimum_required(VERSION 3.12)
project(dsoftbus-example)

set(VERSION_MAJOR 0)
set(VERSION_MINOR 1)

set(CMAKE_BUILD_TYPE Debug)

include(GNUInstallDirs)
find_package(PkgConfig REQUIRED)
pkg_check_modules(softbus_client REQUIRED IMPORTED_TARGET softbus_client)

set(TARGETS src/publish_service.cpp src/discovery.cpp)
set(INCLUDE_DIRS ${PROJECT_SOURCE_DIR}/include/discovery
                ${PROJECT_SOURCE_DIR}/include/dsoftbus)

foreach(file ${TARGETS})
    get_filename_component(target ${file} NAME_WE)
    add_executable(${target} ${file})
    target_link_libraries(${target} PkgConfig::softbus_client)
    target_include_directories(${target} PRIVATE ${INCLUDE_DIRS})
    set_property(TARGET ${target} PROPERTY C_STANDARD 11)
    install(TARGETS ${target} RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})
endforeach()
