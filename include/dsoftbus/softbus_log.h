/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SOFTBUS_LOG_H
#define SOFTBUS_LOG_H

#include <stdio.h>
#include <stdbool.h>

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif


enum {
    SOFTBUS_LOG_LEVEL_DEBUG = 0,
    SOFTBUS_LOG_LEVEL_INFO,
    SOFTBUS_LOG_LEVEL_WARNING,
    SOFTBUS_LOG_LEVEL_ERROR
};

#define SOFTBUS_LOG_LEVEL SOFTBUS_LOG_LEVEL_DEBUG

#define LOG_DBG(fmt, ...) do { \
    if (SOFTBUS_LOG_LEVEL_DEBUG >= SOFTBUS_LOG_LEVEL) { \
        printf("DEBUG:%s:%d " fmt "\n", __FUNCTION__, __LINE__, ##__VA_ARGS__); \
    } \
} while (0)

#define LOG_INFO(fmt, ...) do { \
    if (SOFTBUS_LOG_LEVEL_INFO >= SOFTBUS_LOG_LEVEL) { \
        printf("INFO:%s:%d " fmt "\n", __FUNCTION__, __LINE__, ##__VA_ARGS__); \
    } \
} while (0)

#define LOG_WARN(fmt, ...) do { \
    if (SOFTBUS_LOG_LEVEL_WARNING >= SOFTBUS_LOG_LEVEL) { \
        printf("WARN:%s:%d " fmt "\n", __FUNCTION__, __LINE__, ##__VA_ARGS__); \
    } \
} while (0)

#define LOG_ERR(fmt, ...) do { \
    if (SOFTBUS_LOG_LEVEL_ERROR >= SOFTBUS_LOG_LEVEL) { \
        printf("ERROR:%s:%d " fmt "\n", __FUNCTION__, __LINE__, ##__VA_ARGS__); \
    } \
} while (0)
#endif

#ifdef __cplusplus
#if __cplusplus
}
#endif /* __cplusplus */
#endif /* __cplusplus */